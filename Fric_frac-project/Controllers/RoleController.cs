﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FricFrac.Models.FricFrac;
using Microsoft.EntityFrameworkCore;

namespace FricFrac.Controllers
{
    public class RoleController : Controller
    {
        
        private docent1Context dbContext;
        public RoleController(docent1Context dbContext)
        {
            this.dbContext = dbContext;
        }
        public IActionResult Index()
        {


            ViewBag.Title = "Fric-frac Role Index";
            return View(dbContext.Role.ToList());
        }

        public IActionResult InsertingOne()
        {

            ViewBag.Title = "Fric-frac Role Inserting One";
            return View(dbContext.Role.ToList());

        }

        public IActionResult InsertOne(Models.FricFrac.Role Role)
        {

            ViewBag.Message = "Insert een land in de database";
            dbContext.Role.Add(Role);
            dbContext.SaveChanges();
            return View("Index", dbContext.Role);

        }

        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Insert een land in de database";
            if (id == null)
            {
                return NotFound();
            }

            var Role = dbContext.Role.SingleOrDefault(m => m.Id == id);
            if (Role == null)
            {
                return NotFound();
            }

            return View(Role);
        }

        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac Role Updating One";
            if (id == null)
            {
                return NotFound();
            }

            var Role = dbContext.Role.SingleOrDefault(m => m.Id == id);
            if (Role == null)
            {
                return NotFound();
            }
            return View(Role);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.FricFrac.Role Role)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(Role);
                    dbContext.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.Role.Any(e => e.Id == Role.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", Role);
        }

        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            // zoek het land dat verwijderd moet worden
            var Role = dbContext.Role.SingleOrDefault(m => m.Id == id);
            if (Role == null)
            {
                return NotFound();
            }
            dbContext.Role.Remove(Role);
            dbContext.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
        public IActionResult Cancel()
        {


            return RedirectToAction("Index");
        }


    }


}