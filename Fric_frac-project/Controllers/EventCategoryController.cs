﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FricFrac.Models.FricFrac;
using Microsoft.EntityFrameworkCore;

namespace FricFrac.Controllers
{
    public class EventCategoryController : Controller
    {

        private docent1Context dbContext;
        public EventCategoryController (docent1Context dbContext)
        {
            this.dbContext = dbContext;
        }
        public IActionResult Index()
        {


            ViewBag.Title = "Fric-frac EventCategory Index";
            return View(dbContext.EventCategory.ToList());
        }

        public IActionResult InsertingOne()
        {

            ViewBag.Title = "Fric-frac EventCategory Inserting One";
            return View(dbContext.EventCategory.ToList());

        }

        public IActionResult InsertOne(Models.FricFrac.EventCategory EventCategory)
        {

            ViewBag.Message = "Insert een land in de database";
            dbContext.EventCategory.Add(EventCategory);
            dbContext.SaveChanges();
            return View("Index", dbContext.EventCategory);

        }

        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Insert een land in de database";
            if (id == null)
            {
                return NotFound();
            }

            var EventCategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == id);
            if (EventCategory == null)
            {
                return NotFound();
            }
            return View(EventCategory);
        }

        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac EventCategory Updating One";
            if (id == null)
            {
                return NotFound();
            }

            var EventCategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == id);
            if (EventCategory == null)
            {
                return NotFound();
            }
            return View(EventCategory);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.FricFrac.EventCategory EventCategory)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(EventCategory);
                    dbContext.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.EventCategory.Any(e => e.Id == EventCategory.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", EventCategory);
        }

        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            // zoek het land dat verwijderd moet worden
            var EventCategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == id);
            if (EventCategory == null)
            {
                return NotFound();
            }
            dbContext.EventCategory.Remove(EventCategory);
            dbContext.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
        public IActionResult Cancel()
        {


            return RedirectToAction("Index");
        }











    }
}