#pragma checksum "C:\programmeren3\Fric_frac-final-project\Views\EventTopic\ReadingOne.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a126ce1d547fdd24238ec27940d21cf84c2f3566"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_EventTopic_ReadingOne), @"mvc.1.0.view", @"/Views/EventTopic/ReadingOne.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\programmeren3\Fric_frac-final-project\Views\_ViewImports.cshtml"
using FricFrac;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\programmeren3\Fric_frac-final-project\Views\_ViewImports.cshtml"
using FricFrac.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a126ce1d547fdd24238ec27940d21cf84c2f3566", @"/Views/EventTopic/ReadingOne.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2ff0ac4f61d4da50dea86b36f9fb8c7919ef9c10", @"/Views/_ViewImports.cshtml")]
    public class Views_EventTopic_ReadingOne : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<FricFrac.Models.FricFrac.EventTopic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("show-room entity"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("action", new global::Microsoft.AspNetCore.Html.HtmlString("/EventTopic/Index"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\programmeren3\Fric_frac-final-project\Views\EventTopic\ReadingOne.cshtml"
  
    Layout = "~/Views/Shared/MasterLayout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            WriteLiteral("\r\n\r\n    <nav class=\"control-panel\">\r\n        <a href=\"/Home/Index\" class=\"tile\">\r\n            <i class=\"fas fa-bars\"></i>\r\n            <span class=\"screen-reader-text\">Home</span>\r\n        </a>\r\n        <h1 class=\"banner\">Fric-frac</h1>\r\n    </nav>\r\n\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a126ce1d547fdd24238ec27940d21cf84c2f35664787", async() => {
                WriteLiteral("\r\n    <div class=\"detail\">\r\n        <div class=\"command-panel\">\r\n            <h2 class=\"banner\">Event Topic</h2>\r\n            <a");
                BeginWriteAttribute("href", " href=\"", 557, "\"", 597, 2);
                WriteAttributeValue("", 564, "/EventTopic/UpdatingOne/", 564, 24, true);
#nullable restore
#line 20 "C:\programmeren3\Fric_frac-final-project\Views\EventTopic\ReadingOne.cshtml"
WriteAttributeValue("", 588, Model.Id, 588, 9, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(@" class=""tile"">
                <i class=""fas fa-wrench""></i>
                <i class=""screen-reader-text"">Updating One</i>
            </a>
            <a href=""/EventTopic/InsertingOne"" class=""tile"">
                <i class=""fas fa-plus-circle""></i>
                <i class=""screen-reader-text"">Inserting One</i>
            </a>
            <a");
                BeginWriteAttribute("href", " href=\"", 954, "\"", 992, 2);
                WriteAttributeValue("", 961, "/EventTopic/DeleteOne/", 961, 22, true);
#nullable restore
#line 28 "C:\programmeren3\Fric_frac-final-project\Views\EventTopic\ReadingOne.cshtml"
WriteAttributeValue("", 983, Model.Id, 983, 9, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(@" class=""tile"">
                <i class=""fas fa-trash-alt""></i>
                <i class=""screen-reader-text"">Delete One</i>
            </a>
            <a href=""/EventTopic/Cancel"" class=""tile"">
                <i class=""fas fa-backspace""></i>
                <span class=""screen-reader-text"">Annuleren</span>
            </a>
        </div>


        <fieldset>

            <div>
                <label for=""EventCategory-Name"">Naam: </label>
                <span> ");
#nullable restore
#line 43 "C:\programmeren3\Fric_frac-final-project\Views\EventTopic\ReadingOne.cshtml"
                  Write(Model.Name);

#line default
#line hidden
#nullable disable
                WriteLiteral("</span>\r\n            </div>\r\n            <div>\r\n                <label for=\"EventCategory-Id\"> ID: </label>\r\n                <span> ");
#nullable restore
#line 47 "C:\programmeren3\Fric_frac-final-project\Views\EventTopic\ReadingOne.cshtml"
                  Write(Model.Id);

#line default
#line hidden
#nullable disable
                WriteLiteral(" </span>\r\n            </div>\r\n\r\n\r\n            <a");
                BeginWriteAttribute("href", " href=\"", 1678, "\"", 1716, 2);
                WriteAttributeValue("", 1685, "/EventTopic/DeleteOne/", 1685, 22, true);
#nullable restore
#line 51 "C:\programmeren3\Fric_frac-final-project\Views\EventTopic\ReadingOne.cshtml"
WriteAttributeValue("", 1707, Model.Id, 1707, 9, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(@" class=""tile"">
                <span class=""icon-close""></span>
                <span class=""screen-reader-text"">Delete</span>
            </a>
        </fieldset>



        <div class=""feedback""></div>

    </div>
    <aside class=""list"">
        <table></table>
    </aside>
");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<FricFrac.Models.FricFrac.EventTopic> Html { get; private set; }
    }
}
#pragma warning restore 1591
