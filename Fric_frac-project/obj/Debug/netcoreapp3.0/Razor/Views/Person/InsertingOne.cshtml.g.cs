#pragma checksum "C:\programmeren3\Fric_frac-final-project\Views\Person\InsertingOne.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8b98df8e699a1bfde2b3655b58fb017b0b382089"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Person_InsertingOne), @"mvc.1.0.view", @"/Views/Person/InsertingOne.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\programmeren3\Fric_frac-final-project\Views\_ViewImports.cshtml"
using FricFrac;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\programmeren3\Fric_frac-final-project\Views\_ViewImports.cshtml"
using FricFrac.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8b98df8e699a1bfde2b3655b58fb017b0b382089", @"/Views/Person/InsertingOne.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2ff0ac4f61d4da50dea86b36f9fb8c7919ef9c10", @"/Views/_ViewImports.cshtml")]
    public class Views_Person_InsertingOne : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("show-room entity"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("action", new global::Microsoft.AspNetCore.Html.HtmlString("/Person/InsertOne"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\programmeren3\Fric_frac-final-project\Views\Person\InsertingOne.cshtml"
  
    Layout = "~/Views/Shared/MasterLayout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("    <nav class=\"control-panel\">\r\n        <a href=\"/Home/Index\" class=\"tile\">\r\n            <i class=\"fas fa-bars\"></i>\r\n            <span class=\"screen-reader-text\">Home</span>\r\n        </a>\r\n        <h1 class=\"banner\">Fric-frac</h1>\r\n    </nav>\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "8b98df8e699a1bfde2b3655b58fb017b0b3820894832", async() => {
                WriteLiteral(@"
    <div class=""detail"">
        <div class=""command-panel"">
            <h2 class=""banner"">Persoon</h2>
            <button type=""submit"" value=""/Person/InsertOne"" class=""tile"">
                <i class=""fas fa-user-plus""></i>
                <i class=""screen-reader-text"">Insert One</i>
            </button>

            <a href=""/Person/Index"" class=""tile"">
                <i class=""fas fa-backspace""></i>
                <i class=""screen-reader-text"">Anuuleren</i>
            </a>
        </div>
        <fieldset>
            <div>
                <label for=""Person-FirstName"">Voornaam</label>
                <input id=""Person-FirstName"" name=""Person-FirstName"" type=""text""");
                BeginWriteAttribute("value", " value=\"", 1078, "\"", 1086, 0);
                EndWriteAttribute();
                WriteLiteral(" required />\r\n            </div>\r\n            <div>\r\n                <label for=\"Person-LastName\">Familienaam</label>\r\n                <input id=\"Person-LastName\" name=\"Person-LastName\" type=\"text\"");
                BeginWriteAttribute("value", " value=\"", 1284, "\"", 1292, 0);
                EndWriteAttribute();
                WriteLiteral(" required />\r\n            </div>\r\n            <div>\r\n                <label for=\"Person-Email\">E-mail</label>\r\n                <input id=\"Person-Email\" name=\"Person-Email\" type=\"email\"");
                BeginWriteAttribute("value", " value=\"", 1477, "\"", 1485, 0);
                EndWriteAttribute();
                WriteLiteral(" required />\r\n            </div>\r\n            <div>\r\n                <label for=\"Person-Address1\">Adres 1</label>\r\n                <input id=\"Person-Address1\" name=\"Person-Address1\" type=\"text\"");
                BeginWriteAttribute("value", " value=\"", 1679, "\"", 1687, 0);
                EndWriteAttribute();
                WriteLiteral(" required />\r\n            </div>\r\n            <div>\r\n                <label for=\"Person-Address2\">Adres 2</label>\r\n                <input id=\"Person-Address2\" name=\"Person-Address2\" type=\"text\"");
                BeginWriteAttribute("value", " value=\"", 1881, "\"", 1889, 0);
                EndWriteAttribute();
                WriteLiteral(" required />\r\n            </div>\r\n            <div>\r\n                <label for=\"Person-PostalCode\">Postcode</label>\r\n                <input id=\"Person-PostalCode\" name=\"Person-PostalCode\" type=\"text\"");
                BeginWriteAttribute("value", " value=\"", 2090, "\"", 2098, 0);
                EndWriteAttribute();
                WriteLiteral(" required />\r\n            </div>\r\n            <div>\r\n                <label for=\"Person-City\">Stad</label>\r\n                <input id=\"Person-City\" name=\"Person-City\" type=\"text\"");
                BeginWriteAttribute("value", " value=\"", 2277, "\"", 2285, 0);
                EndWriteAttribute();
                WriteLiteral(" required/>\r\n            </div>\r\n            <div>\r\n                <label for=\"Person-CountryId\">Land</label>\r\n                <select required id=\"Person-CountryId\" name=\"Person-CountryId\" >\r\n");
#nullable restore
#line 57 "C:\programmeren3\Fric_frac-final-project\Views\Person\InsertingOne.cshtml"
                     foreach (var item in ViewBag.Countries)
                    {
                        

#line default
#line hidden
#nullable disable
                WriteLiteral("                        ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "8b98df8e699a1bfde2b3655b58fb017b0b3820898674", async() => {
#nullable restore
#line 60 "C:\programmeren3\Fric_frac-final-project\Views\Person\InsertingOne.cshtml"
                                                     Write(item.Name);

#line default
#line hidden
#nullable disable
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                BeginWriteTagHelperAttribute();
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __tagHelperExecutionContext.AddHtmlAttribute("required", Html.Raw(__tagHelperStringValueBuffer), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.Minimized);
                BeginWriteTagHelperAttribute();
#nullable restore
#line 60 "C:\programmeren3\Fric_frac-final-project\Views\Person\InsertingOne.cshtml"
                                    WriteLiteral(item.Id);

#line default
#line hidden
#nullable disable
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = __tagHelperStringValueBuffer;
                __tagHelperExecutionContext.AddTagHelperAttribute("value", __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n");
#nullable restore
#line 61 "C:\programmeren3\Fric_frac-final-project\Views\Person\InsertingOne.cshtml"
                    }

#line default
#line hidden
#nullable disable
                WriteLiteral("                </select>\r\n            </div>\r\n            <div>\r\n                <label for=\"Person-Phone1\">Telefoon</label>\r\n                <input id=\"Person-Phone1\" name=\"Person-Phone1\" type=\"text\"");
                BeginWriteAttribute("value", " value=\"", 2895, "\"", 2903, 0);
                EndWriteAttribute();
                WriteLiteral("  required />\r\n            </div>\r\n            <div>\r\n                <label for=\"Person-Birthday\">Geboortedatum</label>\r\n                <input id=\"Person-Birthday\" name=\"Person-Birthday\" type=\"date\"");
                BeginWriteAttribute("value", " value=\"", 3104, "\"", 3112, 0);
                EndWriteAttribute();
                WriteLiteral(" required />\r\n            </div>\r\n            <div>\r\n                <label for=\"Person-Rating\">Tevreden</label>\r\n                <input id=\"Person-Rating\" name=\"Person-Rating\" type=\"range\"");
                BeginWriteAttribute("value", " value=\"", 3302, "\"", 3310, 0);
                EndWriteAttribute();
                WriteLiteral(" required />\r\n            </div>\r\n        </fieldset>\r\n        <div class=\"feedback\"></div>\r\n    </div>\r\n    <aside class=\"list\">\r\n        <table></table>\r\n    </aside>\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
