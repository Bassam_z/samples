#pragma checksum "C:\programmeren3\Fric_frac-final-project\Views\Person\UpdatingOne.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5eb5938090ea4109b542c7b1da6e5e69f53e9a40"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Person_UpdatingOne), @"mvc.1.0.view", @"/Views/Person/UpdatingOne.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\programmeren3\Fric_frac-final-project\Views\_ViewImports.cshtml"
using FricFrac;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\programmeren3\Fric_frac-final-project\Views\_ViewImports.cshtml"
using FricFrac.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5eb5938090ea4109b542c7b1da6e5e69f53e9a40", @"/Views/Person/UpdatingOne.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2ff0ac4f61d4da50dea86b36f9fb8c7919ef9c10", @"/Views/_ViewImports.cshtml")]
    public class Views_Person_UpdatingOne : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<FricFrac.Models.FricFrac.Person>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("show-room entity"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("action", new global::Microsoft.AspNetCore.Html.HtmlString("/Person/UpdateOne"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\programmeren3\Fric_frac-final-project\Views\Person\UpdatingOne.cshtml"
  
    Layout = "~/Views/Shared/MasterLayout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("    <nav class=\"control-panel\">\r\n        <a href=\"/Home/Index\" class=\"tile\">\r\n            <i class=\"fas fa-bars\"></i>\r\n            <span class=\"screen-reader-text\">Home</span>\r\n        </a>\r\n        <h1>Fric-frac</h1>\r\n    </nav>\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "5eb5938090ea4109b542c7b1da6e5e69f53e9a404833", async() => {
                WriteLiteral(@"
    <div class=""detail"">
        <div class=""command-panel"">
            <h2 class=""banner"">Persoon</h2>
            <button type=""submit"" class=""tile"">
                <h3>Update One</h3>
            </button>

            <a href=""/Person/Cancel"" class=""tile"">
                <i class=""fas fa-backspace""></i>
                <i class=""screen-reader-text"">Annuleren</i>
            </a>
        </div>
        <fieldset>
            <input type=""hidden"" id=""Person-Id"" name=""Person-Id""");
                BeginWriteAttribute("value", " value=\"", 904, "\"", 921, 1);
#nullable restore
#line 26 "C:\programmeren3\Fric_frac-final-project\Views\Person\UpdatingOne.cshtml"
WriteAttributeValue("", 912, Model.Id, 912, 9, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n\r\n            <div>\r\n                <label for=\"Person-FirstName\">Voornaam</label>\r\n                <input id=\"Person-FirstName\" name=\"Person-FirstName\" type=\"text\"");
                BeginWriteAttribute("value", " value=\"", 1092, "\"", 1116, 1);
#nullable restore
#line 30 "C:\programmeren3\Fric_frac-final-project\Views\Person\UpdatingOne.cshtml"
WriteAttributeValue("", 1100, Model.FirstName, 1100, 16, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" required />\r\n            </div>\r\n            <div>\r\n                <label for=\"Person-LastName\">Familienaam</label>\r\n                <input id=\"Person-LastName\" name=\"Person-LastName\" type=\"text\"");
                BeginWriteAttribute("value", " value=\"", 1314, "\"", 1337, 1);
#nullable restore
#line 34 "C:\programmeren3\Fric_frac-final-project\Views\Person\UpdatingOne.cshtml"
WriteAttributeValue("", 1322, Model.LastName, 1322, 15, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" required />\r\n            </div>\r\n            <div>\r\n                <label for=\"Person-Email\">E-mail</label>\r\n                <input id=\"Person-Email\" name=\"Person-Email\" type=\"email\"");
                BeginWriteAttribute("value", " value=\"", 1522, "\"", 1542, 1);
#nullable restore
#line 38 "C:\programmeren3\Fric_frac-final-project\Views\Person\UpdatingOne.cshtml"
WriteAttributeValue("", 1530, Model.Email, 1530, 12, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n            </div>\r\n            <div>\r\n                <label for=\"Person-Address1\">Adres 1</label>\r\n                <input id=\"Person-Address1\" name=\"Person-Address1\" type=\"text\"");
                BeginWriteAttribute("value", " value=\"", 1727, "\"", 1750, 1);
#nullable restore
#line 42 "C:\programmeren3\Fric_frac-final-project\Views\Person\UpdatingOne.cshtml"
WriteAttributeValue("", 1735, Model.Address1, 1735, 15, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n            </div>\r\n            <div>\r\n                <label for=\"Person-Address2\">Adres 2</label>\r\n                <input id=\"Person-Address2\" name=\"Person-Address2\" type=\"text\"");
                BeginWriteAttribute("value", " value=\"", 1935, "\"", 1958, 1);
#nullable restore
#line 46 "C:\programmeren3\Fric_frac-final-project\Views\Person\UpdatingOne.cshtml"
WriteAttributeValue("", 1943, Model.Address2, 1943, 15, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n            </div>\r\n            <div>\r\n                <label for=\"Person-PostalCode\">Postcode</label>\r\n                <input id=\"Person-PostalCode\" name=\"Person-PostalCode\" type=\"text\"");
                BeginWriteAttribute("value", " value=\"", 2150, "\"", 2175, 1);
#nullable restore
#line 50 "C:\programmeren3\Fric_frac-final-project\Views\Person\UpdatingOne.cshtml"
WriteAttributeValue("", 2158, Model.PostalCode, 2158, 17, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n            </div>\r\n            <div>\r\n                <label for=\"Person-City\">Stad</label>\r\n                <input id=\"Person-City\" name=\"Person-City\" type=\"text\"");
                BeginWriteAttribute("value", " value=\"", 2345, "\"", 2364, 1);
#nullable restore
#line 54 "C:\programmeren3\Fric_frac-final-project\Views\Person\UpdatingOne.cshtml"
WriteAttributeValue("", 2353, Model.City, 2353, 11, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n            </div>\r\n            <div>\r\n                <label for=\"Person-CountryId\">Land</label>\r\n                <select id=\"Person-CountryId\" name=\"Person-CountryId\">\r\n");
#nullable restore
#line 59 "C:\programmeren3\Fric_frac-final-project\Views\Person\UpdatingOne.cshtml"
                     foreach (var item in ViewBag.Countries)
                    {

#line default
#line hidden
#nullable disable
                WriteLiteral("                        ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "5eb5938090ea4109b542c7b1da6e5e69f53e9a4010464", async() => {
                    WriteLiteral("\r\n                            ");
#nullable restore
#line 63 "C:\programmeren3\Fric_frac-final-project\Views\Person\UpdatingOne.cshtml"
                       Write(item.Name);

#line default
#line hidden
#nullable disable
                    WriteLiteral("\r\n                        ");
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
                BeginWriteTagHelperAttribute();
#nullable restore
#line 61 "C:\programmeren3\Fric_frac-final-project\Views\Person\UpdatingOne.cshtml"
                           WriteLiteral(item.Id);

#line default
#line hidden
#nullable disable
                __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
                __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value = __tagHelperStringValueBuffer;
                __tagHelperExecutionContext.AddTagHelperAttribute("value", __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper.Value, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "selected", 1, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
#nullable restore
#line 62 "C:\programmeren3\Fric_frac-final-project\Views\Person\UpdatingOne.cshtml"
AddHtmlAttributeValue("", 2718, item.Id == Model.CountryId ? true : false, 2718, 44, false);

#line default
#line hidden
#nullable disable
                EndAddHtmlAttributeValues(__tagHelperExecutionContext);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral("\r\n");
#nullable restore
#line 65 "C:\programmeren3\Fric_frac-final-project\Views\Person\UpdatingOne.cshtml"
                    }

#line default
#line hidden
#nullable disable
                WriteLiteral("                </select>\r\n            </div>\r\n            <div>\r\n                <label for=\"Person-Phone1\">Telefoon</label>\r\n                <input id=\"Person-Phone1\" name=\"Person-Phone1\" type=\"text\"");
                BeginWriteAttribute("value", " value=\"", 3065, "\"", 3086, 1);
#nullable restore
#line 70 "C:\programmeren3\Fric_frac-final-project\Views\Person\UpdatingOne.cshtml"
WriteAttributeValue("", 3073, Model.Phone1, 3073, 13, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n            </div>\r\n            <div>\r\n                <label for=\"Person-Birthday\">Geboortedatum</label>\r\n                <input id=\"Person-Birthday\" name=\"Person-Birthday\" type=\"date\"");
                BeginWriteAttribute("value", "\r\n                       value=\"", 3277, "\"", 3353, 1);
#nullable restore
#line 75 "C:\programmeren3\Fric_frac-final-project\Views\Person\UpdatingOne.cshtml"
WriteAttributeValue("", 3309, Model.Birthday.Value.ToString("yyyy-MM-dd"), 3309, 44, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n            </div>\r\n        </fieldset>\r\n        <div class=\"feedback\"></div>\r\n    </div>\r\n    <aside class=\"list\">\r\n        <table></table>\r\n    </aside>\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<FricFrac.Models.FricFrac.Person> Html { get; private set; }
    }
}
#pragma warning restore 1591
